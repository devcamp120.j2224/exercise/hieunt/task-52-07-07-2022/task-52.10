import java.util.ArrayList;

import com.devcamp.j53_basicjava.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        ArrayList<Person> listPersons = new ArrayList<Person>();
        Person person1 = new Person();
        Person person2 = new Person("Tran Van Tung", 35, 40);
        Person person3 = new Person("Nguyen Thi Thuan", 25, 40, 15000000);
        Person person4 = new Person("Duong Van Bach", 28, 50, new String[] {"Cat", "Dog"});
        Person person5 = new Person("Le Tuan Anh", 36, 65, 20000000, new String[] {"Cat", "Dog", "Duck"});
 
        //Add cac person vao arraylist
        listPersons.add(person1);
        listPersons.add(person2);
        listPersons.add(person3);
        listPersons.add(person4);
        listPersons.add(person5);
        
        
        for(Person person : listPersons) {
            //person.showInfo();
            System.out.println(person);
        }
    }
}
